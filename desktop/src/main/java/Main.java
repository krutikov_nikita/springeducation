import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.sbt.internetshop.dao.HibernateItemDAO;
import ru.sbt.internetshop.model.Item;

/**
 * Created by SBT-Krutikov-NO on 06.07.2016.
 */
public class Main {

    public static void main(String[] args){
        //ДОСТАТЬ БИНЫ СЕРВИСА И ПОИГРАТЬСЯ С НИМИ

        ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");

        HibernateItemDAO hibernateItemDAO = (HibernateItemDAO) context.getBean("hibernateItemDAO");
        hibernateItemDAO.save(new Item("Carrot", 12.5));
        hibernateItemDAO.save(new Item("Potato", 1.25));
        hibernateItemDAO.save(new Item("Cucumber", 0.125));
    }

}
