package ru.sbt.internetshop.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.sbt.internetshop.model.Item;

import java.io.Serializable;
import java.util.List;

/**
 * Created by SBT-Krutikov-NO on 07.07.2016.
 */
//@Repository
public class HibernateItemDAO implements AbstractDAO<Item>{

    private SessionFactory sessionFactory;

    //@Autowired
//    public HibernateItemDAO(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }

//    public HibernateItemDAO(){}

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession(){
        return sessionFactory.getCurrentSession();
    }

    public boolean update(Item object) {
        try {
            currentSession().update(object);
            return true;
        } catch (HibernateException e){
            //DoSomething
            System.out.println(e);
            return false;
        }
    }

    public boolean save(Item object) {
        try {
            currentSession().save(object);
            return true;
        } catch (HibernateException e) {
            //DoSomething
            System.out.println(e);
            return false;
        }
    }

    public Item read(Serializable id) {
        try {
            return (Item)currentSession().get(Item.class, id);
        } catch (HibernateException e) {
            //DoSomething
            System.out.println(e);
            return null;
        }
    }

    public List<Item> readAll() {
        Criteria criteria = currentSession().createCriteria(Item.class);
        return (List<Item>) criteria.list();
    }

    public boolean delete(Item object) {
        try {
            currentSession().delete(object);
            return true;
        } catch (HibernateException e) {
            //DoSomething
            System.out.println(e);
            return false;
        }
    }
}
