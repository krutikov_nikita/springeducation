package ru.sbt.internetshop.dao;

import ru.sbt.internetshop.model.BaseEntity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by SBT-Krutikov-NO on 06.07.2016.
 */
public interface AbstractDAO<T extends BaseEntity> {

    boolean update(T object);

    boolean save(T object);

    T read(Serializable id);

    List<T> readAll();

    boolean delete(T object);
}
