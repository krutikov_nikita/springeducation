package ru.sbt.internetshop.dao;

/**
 * Created by SBT-Krutikov-NO on 06.07.2016.
 */

import ru.sbt.internetshop.model.Order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * make bean with declaration
 */
public class OrderDAO implements AbstractDAO<Order> {
    static Map<Long, Order> localStorage = new HashMap<Long, Order>();
    private static Long currentId = 0l;

    private Long createId(){
        return ++currentId;
    }

    public boolean update(Order object) {
        if (object.getId()==null) object.setId(createId());

        if (localStorage.put(object.getId(), object)!=null){
            return true;
        } else {
            return false;
        }
    }

    public boolean save(Order object) {
        return update(object);
    }

    public Order read(Serializable id) {
        return localStorage.get(id);
    }

    public List<Order> readAll() {
        return new ArrayList<Order>(localStorage.values());
    }

    public boolean delete(Order object) {
        if (localStorage.remove(object.getId())!=null){
            return true;
        } else {
            return false;
        }
    }
}

