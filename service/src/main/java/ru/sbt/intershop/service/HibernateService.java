package ru.sbt.intershop.service;

import ru.sbt.internetshop.dao.HibernateItemDAO;
import ru.sbt.internetshop.model.Item;

import java.util.List;

/**
 * Created by SBT-Krutikov-NO on 07.07.2016.
 */
//@Service
public class HibernateService implements AbstractService {

    //@Autowired
    private HibernateItemDAO hibernateItemDAO;

    public void setHibernateItemDAO(HibernateItemDAO hibernateItemDAO) {
        this.hibernateItemDAO = hibernateItemDAO;
    }

    public HibernateItemDAO getHibernateItemDAO() {
        return hibernateItemDAO;
    }

    public List<Item> getItems() {
        return hibernateItemDAO.readAll();
    }

    public void addNewItem(Item item) {
        hibernateItemDAO.save(item);
    }

    public void updateItem(Item item) {
        hibernateItemDAO.update(item);
    }
}
