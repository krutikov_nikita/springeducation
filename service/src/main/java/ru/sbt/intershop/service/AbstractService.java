package ru.sbt.intershop.service;

import ru.sbt.internetshop.model.Item;

import java.util.List;

/**
 * Created by SBT-Krutikov-NO on 07.07.2016.
 */
public interface AbstractService {
    List<Item> getItems();
    void addNewItem(Item item);
    void updateItem(Item item);
}
