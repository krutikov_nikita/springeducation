package ru.sbt.internetshop.model;

import java.io.Serializable;

/**
 * Created by SBT-Krutikov-NO on 06.07.2016.
 */
public class BaseEntity implements Serializable {

    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
