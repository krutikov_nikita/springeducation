package ru.sbt.internetshop.model;

//@Entity
//@Table(name="ITEMS")
public class Item extends BaseEntity {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

//    @Column(name = "ITEM_NAME", nullable = false)
    private String name;

//    @Column(name = "ITEM_PRICE", nullable = false)
    private double price;

    public Item(String name, double price) {
        this.name = name;
        this.price = price;
        this.id = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Item)) return false;
        Item other = (Item) obj;
        if (id != other.id) return false;
        if (price != other.price) return false;
        if (name != other.name) return false;
        return true;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Item #" + id + "\n");
        builder.append("Name: \""+ name + "\"\n");
        builder.append("Price: "+ price + "\n");
        return builder.toString();
    }
}
