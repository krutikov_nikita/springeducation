package ru.sbt.internetshop.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SBT-Krutikov-NO on 06.07.2016.
 */
public class Order extends BaseEntity {

    private List<Item> items;

    public Order(){
        this.items=new ArrayList<Item>();
    }

    public Order(List<Item> items){
        this.items=items;
    }

    List<Item> getItems() {
        return items;
    }

    public boolean addItem(Item item) {
        return items.add(item);
    }

    public boolean removeItem(Item item) {
        return items.remove(items);
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}

